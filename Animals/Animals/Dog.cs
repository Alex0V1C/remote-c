﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Animals
{
    class Dog : Animal
    {
        public Dog()
        {
            this.Name = "Dog";
            this.Sound = "woof";
        }

    }
}
