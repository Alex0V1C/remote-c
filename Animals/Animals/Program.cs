﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Animals;

namespace animals
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> myList = new List<Animal>();
            myList.Add(new Cat());
            myList.Add(new Dog());
            myList.Add(new Duck());
            myList.Add(new Duck());
            myList.Add(new Dog());
            myList.Add(new Duck());
      
            foreach (Animal it in myList)
            {
                 it.display();
            }

            Console.ReadKey();
            }
    }
}
