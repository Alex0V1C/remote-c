﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Animals
{
    class Duck : Animal
    {
        public Duck()
        {
            this.Name = "Duck";
            this.Sound = "quack";
        }
    }
}
