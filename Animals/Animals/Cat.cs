﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Animals
{
    class Cat : Animal
    {
        public Cat()
        {
            this.Name = "Cat";
            this.Sound = "mew";
        }
    }
}
