﻿using System;

namespace Animals
{
    class Animal
    {
        private string _name;
        private string _sound;

        protected string Name
        {
            set
            {
                this._name = value;
            }
            get
            {
                return this._name;
            }
        }

        protected string Sound
        {
            set
            {
                this._sound = value;
            }
            get
            {
                return this._sound;
            }
        }

        public void display()
        {
            Console.WriteLine(this.Name + " goes " + this.Sound);
        }
    }
}
