﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    class ContainableItemsCollection
    {
        Dictionary<Position, ContainableItem> collection = new Dictionary<Position, ContainableItem>();

        public void AddProduct(ContainableItem contItem)
        {
            bool ok = true;

            foreach (KeyValuePair<Position, ContainableItem> kvp in collection)
                if (kvp.Value.position.row == contItem.position.row && kvp.Value.position.column == contItem.position.column)
                {
                    Console.WriteLine("This location is occupied by another product!!!\n");
                    ok = false;
                }

            if (ok == true)
                this.collection[contItem.position] = contItem;
        }

        public void RemoveProduct(int ID1)
        {
            var ans = this.collection.FirstOrDefault(product => product.Value.GetID == ID1);

            if (ans.Key != null)
            {
                this.collection.Remove(ans.Key);
            }
            else
            {
                Console.WriteLine("ID not found");
            }
        }

        public void RemoveProductUsingPosition(Position position)
        {
            var ans = this.collection.FirstOrDefault(product => (product.Key.row == position.row && product.Key.column == position.column));

            if (ans.Key != null)
                collection.Remove(ans.Key);
            else
                Console.WriteLine("This location is empty");

        }

        public void ViewProducts()
        {
            try
            {
                var items = from pair in collection
                            orderby pair.Value ascending
                            select pair.Key.row;
                foreach (KeyValuePair<Position, ContainableItem> kvp in collection)
                    kvp.Value.ViewContainableItem();
            }
            catch
            {
                Console.Write("\nThe function can not display the products");
            }
        }

        public void ViewSpecificProduct(int ID1)
        {
            var ans = this.collection.FirstOrDefault(product => product.Value.GetID == ID1);

            if (ans.Key != null)
                ans.Value.ViewContainableItem();
            else Console.WriteLine("ID not found");

        }


        public void UpdateQuantity(int ID1, int plus)
        {
            var ans = this.collection.FirstOrDefault(product => product.Value.GetID == ID1);

            if (ans.Key != null)
            {
                this.collection[ans.Key].Quantity = this.collection[ans.Key].Quantity + plus;

                if (this.collection[ans.Key].Quantity == 0)
                {
                    RemoveProduct(ID1);
                }
            }
            else
                Console.WriteLine("ID not found");

        }

        public void DisplayProductsforClients()
        {
            try
            {
                var items = from pair in collection
                            orderby pair.Value ascending
                            select pair.Key.row;
                foreach (KeyValuePair<Position, ContainableItem> kvp in collection)
                    kvp.Value.DisplayContainableItemForClient();
            }
            catch
            {
                Console.Write("\nThe function can not display the products");
            }
        }

        public KeyValuePair<Position, ContainableItem> SearchProduct(int ID1)
        {
            var ans = this.collection.FirstOrDefault(product => product.Value.GetID == ID1);

            return ans;
        }
    }
}
