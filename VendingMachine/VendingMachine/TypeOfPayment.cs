﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    interface TypeOfPayment
    {
        bool Transfer(int ID, ref decimal sum, ContainableItemsCollection collection, PaymentTerminal aux);
    }
}
