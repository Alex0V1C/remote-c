﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    class Dispenser
    {
        public Dispenser() { }

        public void DeleteItem(int ID, ContainableItemsCollection collection)
        {
            collection.UpdateQuantity(ID, -1);
        }
    }
}
