﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    class CardPayment: TypeOfPayment
    {
        public CardPayment(){}

        public bool Transfer(int ID, ref decimal totalSum, ContainableItemsCollection collections, PaymentTerminal aux)
        {
            return aux.Valid(ID, ref totalSum, collections);
        }
    }
}
