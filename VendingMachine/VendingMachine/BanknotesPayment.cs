﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    class BanknotesPayment: TypeOfPayment
    {
        public BanknotesPayment(){}

        public bool Transfer(int ID, ref decimal totalSum, ContainableItemsCollection collection, PaymentTerminal aux)
        {
            return aux.Valid(ID, ref totalSum, collection);
        }
    }
}
