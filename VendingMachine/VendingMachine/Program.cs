﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    class Program
    {
        public void CreateVendingMachine(ContainableItemsCollection p)
        {
            try
            {
                using (StreamReader inputFile = new StreamReader("inputData.txt"))
                {
                    string line;
                    char[] delimeterChars = { ':', ',', '\t' };

                    while ((line = inputFile.ReadLine()) != null)
                    {
                        string[] data = line.Split(delimeterChars);

                        int option = Convert.ToInt32(data[0]);

                        if (option == 1)
                        {
                            string name = Convert.ToString(data[1]);
                            int ID = Convert.ToInt32(data[2]);
                            string type = Convert.ToString(data[3]);
                            decimal price = Convert.ToDecimal(data[4]);
                            int quantity = Convert.ToInt32(data[5]);
                            int size = Convert.ToInt32(data[6]);
                            int row = Convert.ToInt32(data[7]);
                            int column = Convert.ToInt32(data[8]);

                            Product prod = new Product(name, ID, type, price, quantity, size);
                            Position pos = new Position(row, column);
                            ContainableItem cntItem = new ContainableItem(prod, pos);

                            p.AddProduct(cntItem);
                        }
                        else if (option == 2)
                        {
                            int ID = Convert.ToInt32(data[1]);

                            p.RemoveProduct(ID);
                        }
                        else if (option == 3)
                        {
                            int row = Convert.ToInt32(data[1]);
                            int column = Convert.ToInt32(data[2]);
                            Position position = new Position(row, column);

                            p.RemoveProductUsingPosition(position);
                        }
                        else if (option == 4)
                        {
                            p.ViewProducts();
                        }
                        else if (option == 5)
                        {
                            int ID = Convert.ToInt32(data[1]);

                            p.ViewSpecificProduct(ID);
                        }
                        else if (option == 6)
                        {
                            int ID = Convert.ToInt32(data[1]);
                            int size = Convert.ToInt32(data[2]);

                            p.UpdateQuantity(ID, size);
                        }
                    }

                    inputFile.Close();
                }
            }
            catch
            {
                bool ok = true;

                while (ok == true)
                {
                    Console.WriteLine("\n1. Add Product");
                    Console.WriteLine("2. Remove Product");
                    Console.WriteLine("3. Remove Product Using Position ");
                    Console.WriteLine("4. View Products");
                    Console.WriteLine("5. View Specific Product");
                    Console.WriteLine("6. Add Quantity");
                    Console.WriteLine("7. Exit");

                    Console.Write("Option: ");
                    int option = int.Parse(Console.ReadLine());
                    int ID, row1, column1;
                    Position position = new Position();

                    switch (option)
                    {
                        case 1:
                            Console.WriteLine("Enter the data:");

                            Console.Write("Name: ");
                            string name1 = Console.ReadLine();

                            Console.Write("ID: ");
                            int ID1 = Convert.ToInt32(int.Parse(Console.ReadLine()));

                            Console.Write("Type: ");
                            string type1 = Console.ReadLine();

                            Console.Write("Price: ");
                            decimal price1 = Convert.ToDecimal(decimal.Parse(Console.ReadLine()));

                            Console.Write("Quantity: ");
                            int quantity1 = Convert.ToInt32(int.Parse(Console.ReadLine()));

                            Console.Write("Size: ");
                            int size1 = Convert.ToInt32(int.Parse(Console.ReadLine()));

                            Console.Write("Position: \n");
                            Console.Write("     Row = ");
                            row1 = Convert.ToInt32(int.Parse(Console.ReadLine()));
                            Console.Write("     Column = ");
                            column1 = Convert.ToInt32(int.Parse(Console.ReadLine()));

                            Product prod = new Product(name1, ID1, type1, price1, quantity1, size1);
                            Position pos = new Position(row1, column1);
                            ContainableItem cntItem = new ContainableItem(prod, pos);


                            p.AddProduct(cntItem);
                            break;

                        case 2:
                            Console.Write("Enter ID: ");
                            ID = int.Parse(Console.ReadLine());
                            p.RemoveProduct(ID);
                            break;
                        case 3:
                            Console.Write("Enter Position: \n");
                            Console.Write("     Row = ");
                            row1 = Convert.ToInt32(int.Parse(Console.ReadLine()));
                            Console.Write("     Column = ");
                            column1 = Convert.ToInt32(int.Parse(Console.ReadLine()));

                            position = new Position(row1, column1);

                            p.RemoveProductUsingPosition(position);
                            break;
                        case 4:
                            p.ViewProducts();
                            break;

                        case 5:
                            Console.Write("Enter ID: ");
                            ID = int.Parse(Console.ReadLine());
                            p.ViewSpecificProduct(ID);
                            break;
                        case 6:
                            Console.Write("Enter ID: ");
                            ID = int.Parse(Console.ReadLine());
                            Console.Write("Enter plus: ");
                            int plus = int.Parse(Console.ReadLine());
                            p.UpdateQuantity(ID, plus);
                            break;

                        default:
                            ok = false;
                            break;
                    }
                }
            }
        }

        public void BuyProducts()
        {
            ContainableItemsCollection containableItems = new ContainableItemsCollection();
            CreateVendingMachine(containableItems);
            Dispenser dispenser = new Dispenser();

            int option;
            bool ok = true;

            while (ok == true)
            {
				Console.ReadKey();
				Console.Clear();
				containableItems.DisplayProductsforClients();
				Console.WriteLine("\n1. Buy a product");
                Console.WriteLine("2. Exit");

                Console.Write("Your option: ");
                option = int.Parse(Console.ReadLine());

                switch (option)
                {
                    case 1:
						Console.WriteLine("     Enter the ID of product that you want to buy: ");
                        int ID = int.Parse(Console.ReadLine());
						PaymentTerminal paymentTerminal = new PaymentTerminal();

						var auxContItem = containableItems.SearchProduct(ID);

                        if (auxContItem.Key == null)
                            Console.WriteLine("ID not found");
                        else
                        {
                            decimal productPrice = auxContItem.Value.GetPrice;

                            Console.WriteLine("     Choose payment type:");
                            Console.WriteLine("         1-card, 2-banknotes, 3-coins");

                            int type = int.Parse(Console.ReadLine());
                            string typeOfPayment;

                            if (type == 1)
                                typeOfPayment = "card";
                            else if (type == 2)
                                typeOfPayment = "banknotes";
                            else typeOfPayment = "coins";

                            decimal amountOfMoney = 0;

                            if (typeOfPayment == "card")
                            {
                                Console.WriteLine("     Enter the card in the tonomat: ");
                                CardPayment card = new CardPayment();
								amountOfMoney = auxContItem.Value.GetPrice;

								if (card.Transfer(ID, ref amountOfMoney, containableItems, paymentTerminal) == true)
                                {
                                    dispenser.DeleteItem(ID, containableItems);
                                    Console.WriteLine("Pick up the product");
                                }
                            }
                            else if(typeOfPayment == "banknotes")
                            {
                                Console.WriteLine("     1. Enter the money in the tonomat: ");
                                amountOfMoney = int.Parse(Console.ReadLine());
                                BanknotesPayment banknotes = new BanknotesPayment();
                                int nr = 1;
                                bool temp = banknotes.Transfer(ID, ref amountOfMoney, containableItems, paymentTerminal);

                                while (!temp && nr <3)
                                {
                                    ++nr;
                                    Console.WriteLine("     " + nr + ". Enter the rest of money in the tonomat: ");
                                    amountOfMoney += int.Parse(Console.ReadLine());
                                    temp = banknotes.Transfer(ID, ref amountOfMoney, containableItems, paymentTerminal);
                                }

                                if(nr >= 3 && temp == false)
                                {
                                    Console.WriteLine("     Time is over. The sale failed");
                                }
                                else
                                {
                                    dispenser.DeleteItem(ID, containableItems);
                                    Console.WriteLine("Pick up the product");
                                }
                            }
                            else
                            {
                                Console.WriteLine("     1. Enter the money in the tonomat: ");
                                amountOfMoney = int.Parse(Console.ReadLine());
                                CoinsPayment coins = new CoinsPayment();
                                int nr = 1;
                                bool temp = coins.Transfer(ID, ref amountOfMoney, containableItems, paymentTerminal);

                                while (!temp && nr < 3)
                                {
                                    ++nr;
                                    Console.WriteLine("     " + nr + ". Enter the rest of money in the tonomat: ");
                                    amountOfMoney += int.Parse(Console.ReadLine());
									amountOfMoney /= 10;

									temp = coins.Transfer(ID, ref amountOfMoney, containableItems, paymentTerminal);
                                }

                                if (nr >= 3 && temp == false)
                                {
                                    Console.WriteLine("     Time is over. The sale failed");
                                }
                                else
                                {
                                    dispenser.DeleteItem(ID, containableItems);
                                    Console.WriteLine("Pick up the product");
                                }
                            }
                        }
                        break;

                    default:
                        ok = false;
                        break;
                }
            }
        }

        static void Main(string[] args)
        {
            Program vendingMachine = new Program();

            //ContainableItemsCollection collection = new ContainableItemsCollection();
            vendingMachine.BuyProducts();

        }
    }
}