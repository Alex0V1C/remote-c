﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    class Position
    {
        public int row, column;

        public Position() { }

        public Position(int row, int column)
        {
            this.row = row;
            this.column = column;
        }
    }
}
