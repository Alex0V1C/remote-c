﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    class ContainableItem: Product
    {
        public Position position;
        public Product product;

        public ContainableItem() { }

        public ContainableItem(Product product, Position position)
        {
            this.product = product;
            this.position = position;
        }

        public int GetID { get { return this.product.ID; } }
        public int Quantity { get { return this.product.quantity; } set { this.product.quantity = value; } }
        public decimal GetPrice { get { return this.product.GetPrice; } }

        public void ViewContainableItem()
        {
            try
            {
                product.ViewProduct();
                Console.Write("\nPosition: " + "row = " + this.position.row + " column = " + this.position.column + "\n");

            }
            catch
            {
                Console.Write("\nThe function can not display the Containable Item\n");
            }
        }

        public void DisplayContainableItemForClient()
        {
            try
            {
                product.DisplayProductForClient();
                Console.WriteLine();
            }
            catch
            {
                Console.Write("\nThe function can not display the Containable Item\n");
            }
        }
    }
}
