﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    class PaymentTerminal
    {
        public PaymentTerminal() { }

        public bool Valid(int ID, ref decimal sum, ContainableItemsCollection collections)
        {
            var aux = collections.SearchProduct(ID);
            if(aux.Value.GetPrice == sum)
            {
                Console.WriteLine("The amount of money you entered is correct");
                return true;
            }
            else if(aux.Value.GetPrice > sum)
            {
                Console.WriteLine("Insufficient money. Please add more money");
                return false;
            }
            Console.WriteLine("You have introduced more money, please lift the rest");
            return true;
        }
    }
}
