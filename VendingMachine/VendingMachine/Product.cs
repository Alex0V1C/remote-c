﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    class Product
    {
        private string name;
        protected internal decimal GetPrice;
        private string type;
        protected internal int quantity;
        protected internal int ID;
        private int size;

        protected Product() { }

        public Product(string name, int ID, string type, decimal price, int quantity, int size)
        {
            this.name = name;
            this.GetPrice = price;
            this.type = type;
            this.quantity = quantity;
            this.ID = ID;
            this.size = size;
        }

        public void ViewProduct()
        {
            try
            {
                Console.Write("\nProduct: " + this.name);
                Console.Write("\nID: " + this.ID);
                Console.Write("\nType of products: " + this.type);
                Console.Write("\nPrice: " + this.GetPrice);
                Console.Write("\nQuantity: " + this.quantity);
                Console.Write("\nSize: " + this.size);
            }
            catch
            {
                Console.Write("The function can not display the product\n");
            }
        }

        public void DisplayProductForClient()
        {
            try
            {
                Console.Write("\nProduct: " + this.name);
                Console.Write("\nID: " + this.ID);
                Console.Write("\nType of products: " + this.type);
                Console.Write("\nPrice: " + this.GetPrice);
            }
            catch
            {
                Console.Write("The function can not display the product\n");
            }
        }
    }
}
