﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project
{
    class Program
    {
        static void Main(string[] args)
        {
            GrupStudents grup1 = new GrupStudents();
            grup1.ReadData();
            grup1.DisplayList();

            var x = grup1.SearchStudentByName("Victor");
            //[Alexandra]Daca x e null si incerc sa-l afisez am eroare
            if (x != null)
            {
                x.DisplayStudent();
            }

            grup1.SortByName();
            grup1.DisplayList();

            grup1.SortDescendingtByGrade();
            grup1.DisplayList();

            var y = grup1.SearchStudentsByGrade(9);
            //[Alexandra]Daca y e null si incerc sa-l afisez am eroare
            if (y != null)
            {
                foreach (Student s in y)
                {
                    s.DisplayStudent();
                }
            }

            Console.ReadKey();
        }
    }
}
