﻿using System;
using System.IO;

namespace Students
{
    class Student
    {
        private string name;
        private double grade;
        private int nrDroppedExams;

        protected Student() { }

        protected internal Student(string name, double grade, int nrDroppedExams)
        {
            this.name = name;
            this.grade = grade;
            this.nrDroppedExams = nrDroppedExams;
        }

        // [Andreea] nu ai nevoie de Get la numele proprietatilor, le pui direct Nume, Grade, etc
        public string Name { get { return name; } }

        public double Grade { get { return grade; } }

        public int NrDroopedExams { get { return nrDroppedExams; } }

        public void DisplayStudent()
        {
            try
            {
                using (var outputFile = new StreamWriter("OutputFile/outputFile.txt", true))
                {
                    outputFile.WriteLine(this.name + ": " + this.grade + ", " + this.nrDroppedExams);
                    outputFile.Close();
                }
            }
            catch
            {
                Console.WriteLine(this.name + ": " + this.grade + ", " + this.nrDroppedExams);
            }
        }
    }
}
