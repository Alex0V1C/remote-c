﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Students
{
    class GrupStudents:  Student
    {
        private List<Student> grupStudents = new List<Student> ();

        public void ReadData()
        {
            try
            {
                using (StreamReader inputFile = new StreamReader("InputFile/inputFile.txt"))
                {
                    string line;
                    char[] delimiterChars = { ' ', ',', ':', '\t' };

                    while ((line = inputFile.ReadLine()) != null)
                    {
                        string[] data = line.Split(delimiterChars);
                        
                        string name = data[0];
                        double grade = Convert.ToDouble(data[1]);
                        int nrDroppedExams = Convert.ToInt32(data[2]);

                        Student s = new Student(name, grade, nrDroppedExams);
                       
                        grupStudents.Add(s);
                    }

                    inputFile.Close();
                }
            }
            catch
            {
                Console.WriteLine("The reading file is missing");
            }
        }

        public void DisplayList()
        {
            foreach (Student i in this.grupStudents)
            {
                i.DisplayStudent();
            }
        }

        public void SortByName()
        {
            this.grupStudents = this.grupStudents.OrderBy(student => student.Name).ToList();
        }

        public void SortDescendingtByGrade()
        {
            this.grupStudents = this.grupStudents.OrderByDescending(student => student.Grade).ToList();
        }
        
        public void SearchStudentByName(string name)
        {
            var ans = this.grupStudents.Find(student => student.Name == name);
           
            if (ans != null)
            {
                ans.DisplayStudent();
            }
            // [Andreea].... hmmm... are you sure? mie imi apare.
            else//[Alexandra]Mesajul nu apare in fisier. Cum pot rezolva problema?
            {
                // [Andreea] - de cate ori facem scrierea in fisier? Ar trebui sa facem ceva in privinta 
                try
                {
                    StreamWriter outputFile = new StreamWriter("OutputFile/outputFile.txt", true);
                    outputFile.WriteLine("There is no student named " + name);
                    outputFile.Close();
                }
                catch
                {
                    Console.WriteLine("There is no student named " + name);
                }
            }
            
        }

        public void  SearchStudentsByGrade(double grade)
        {
            // [Andreea] ??? nu inteleg ce vrei sa faci cu noua lista.
            // mesajul nu apare si nu o sa apara niciodata pentru ca tu creezi o lista de studenti
            // pe care o initializezi cu new list
            // si nu va fi niciodata null
            // ce ar trebui sa faci - il cauti si daca ce iti returneaza cautarea e null, atunci pui in fisier
            if (this.grupStudents.Find(x => x.Grade == grade) != null)
            {
                Console.WriteLine("Students with grade " + grade + " are:");
                foreach (var student in this.grupStudents)
                {
                    if (student.Grade == grade)
                    {
                        student.DisplayStudent();
                    }
                }
            }
            else
            {
                try
                {
                    StreamWriter outputFile = new StreamWriter("OutputFile/outputFile.txt", true);
                    outputFile.WriteLine("No student has the grade " + grade);
                    outputFile.Close();
                }
                catch
                {
                    Console.WriteLine("No student has the grade " + grade);
                }
                
            }
        }
    }
}
