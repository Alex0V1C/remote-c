﻿using System;

namespace Students
{
    class Program
    {
        static void Main(string[] args)
        {
            GrupStudents grup1 = new GrupStudents();
            grup1.ReadData();
            grup1.DisplayList();

            grup1.SearchStudentByName("Victor");

            grup1.SortByName();
            grup1.DisplayList();

            grup1.SortDescendingtByGrade();
            grup1.DisplayList();

            grup1.SearchStudentsByGrade(10);

            Console.ReadKey();
        }
    }
}
