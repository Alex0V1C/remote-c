﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Project
{
    class GrupStudents:  Student
    {
        private System.Collections.Generic.List<Student> lst = new System.Collections.Generic.List<Student> ();

        public void ReadData()
        {
            try
            {
                using (StreamReader inputFile = new StreamReader("D:/Programs/Repository C#/Students/Project/inputFile.txt"))
                {
                    string line;
                    char[] delimiterChars = { ' ', ',', ':', '\t' };

                    while ((line = inputFile.ReadLine()) != null)
                    {
                        string[] data = line.Split(delimiterChars);

                        string name = data[0];
                        double grade = Convert.ToDouble(data[1]);
                        int nrDroppedExams = Convert.ToInt32(data[2]);

                        Student s = new Student(name, grade, nrDroppedExams);
                       
                        lst.Add(s);
                    }

                    inputFile.Close();
                }
            }
            catch
            {
                Console.WriteLine("The reading file is missing");
            }
        }

        public void DisplayList()
        {
            foreach (Student i in lst)
            {
                i.DisplayStudent();
            }
        }

        public void SortByName()
        {
            this.lst = this.lst.OrderBy(student => student.GetName).ToList();
        }

        public void SortDescendingtByGrade()
        {
            this.lst = this.lst.OrderByDescending(student => student.GetGrade).ToList();
        }
        
        public Student SearchStudentByName(string name)
        {
             var ans = this.lst.Find(student => student.GetName == name);

            if (ans != null)
            {
                return ans;
            }
            else//[Alexandra]Mesajul nu apare in fisier. Cum pot rezolva problema?
            {
                StreamWriter outputFile = new StreamWriter("D:/Programs/Repository C#/Students/Project/outputFile.txt", true);
                outputFile.WriteLine("There is no student named " + name);
                outputFile.Close();

                return null;
            }
        }

        public List<Student> SearchStudentsByGrade(double grade)
        {
            var ans = new List<Student>();

            if (ans != null)
            {
                ans = this.lst.FindAll(student => student.GetGrade == grade).ToList();

                return ans;
            }
            else//[Alexandra]Mesajul nu apare in fisier
            {   
                using (var outputFile = new StreamWriter("D:/Programs/Repository C#/Students/Project/outputFile.txt", true))
                {
                    outputFile.WriteLine("There is no student with grade " + grade);
                    
                }

                return null;
            }

        }

    }
}
