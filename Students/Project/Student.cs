﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Project
{
    class Student
    {
        private string name;
        private double grade;
        private int nrDroppedExams;

        protected Student() { }

        //[Alexandra] Nu pot sa folosesc constructorul in class GrupStudents daca il declar protected.
        public Student(string name, double grade, int nrDroppedExams)
        {
            this.name = name;
            this.grade = grade;
            this.nrDroppedExams = nrDroppedExams;
        }

        public string GetName { get { return name; } }

        public double GetGrade { get { return grade; } }

        public int GetNrDroopedExams { get { return nrDroppedExams; } }

        public void DisplayStudent()
        {
            try
            {
                using (var outputFile = new StreamWriter("D:/Programs/Repository C#/Students/Project/outputFile.txt", true))
                {

                    outputFile.WriteLine(this.name + ": " + this.grade + ", " + this.nrDroppedExams);

                   outputFile.Close();
                }
            }
            catch
            {
                Console.WriteLine(this.name + ": " + this.grade + ", " + this.nrDroppedExams);
            }
        }
    }
}
